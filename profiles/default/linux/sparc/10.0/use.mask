# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /data/temp/gentoo//vcs-public-cvsroot/gentoo-x86/profiles/default/linux/sparc/10.0/use.mask,v 1.3 2012/05/26 06:08:34 blueness Exp $

# this is not a multilib profile
multilib

# Anthony G. Basile <blueness@gentoo.org> (15 Apr 2012)
# Pulls in net-libs/axtls which is not keyworded for arch
curl_ssl_axtls
