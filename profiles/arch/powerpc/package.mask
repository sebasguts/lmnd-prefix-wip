# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /data/temp/gentoo//vcs-public-cvsroot/gentoo-x86/profiles/arch/powerpc/package.mask,v 1.51 2012/05/21 11:21:07 alexxy Exp $

# Alexey Shvetsov <alexxy@gentoo.org> (21 May 2012)
# Need deps pecl-apc and pecl-uploadprogress bug #416897
>=www-apps/drupal-7.14

# Johannes Huber <johu@gentoo.org> (21 Feb 2012)
# Mask failing package on powerpc
=kde-base/krossjava-4.7*

# Justin Lecher <jlec@gentoo.org> (9 Mar 2011)
# sci-libs/plplot needs keywords #358035
=sci-biology/emboss-6.3.1*

# Joseph Jezak <josejx@gentoo.org> (12 Feb 2010)
# Masking MOL due to breakage, but leaving it in the tree in case anyone 
# still needs it. Unmask if you want, but there's nothing in the way of support.
app-emulation/mol

# Alexis Ballier <aballier@gentoo.org> (11 Jan 2010)
# Mask TeX Live 2009 for testing
# g-ctan can be unmasked for all arches (only ~amd64 and ~x86)
>=app-portage/g-ctan-2009.1
