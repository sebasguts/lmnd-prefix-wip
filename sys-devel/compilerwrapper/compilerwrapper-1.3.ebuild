# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3
inherit eutils

DESCRIPTION=""
HOMEPAGE="https://bitbucket.org/vbraun/compilerwrapper"
# The repository doesn't have any tags
# this should correspond to the contents of 1.0 spkg
SRC_URI="http://bitbucket.org/vbraun/compilerwrapper/get/d1680a59da36.tar.gz"
S="${WORKDIR}/vbraun-compilerwrapper-d1680a59da36"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64-linux x86-linux"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

src_configure() {
	econf --with-ccpath=/usr/bin --with-ldpath=/usr/bin
}

src_install() {
	emake DESTDIR="${D}" install || die "Install failed"
	dodoc AUTHORS NEWS README TODO
}
